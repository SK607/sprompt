FROM alpine:latest
ARG USERNAME="sk607"
ARG HOMEDIR="/home/$USERNAME"
ARG MOUNTDIR="$HOMEDIR/sprompt"

# add test user
RUN adduser -D "$USERNAME"

# install packages
#   bash    - to test shell support
#   dash    - to test shell support
#   zsh     - to test shell support
#   ash     - to test shell support (available in alpine by default)
#   git     - to test git integration
#   bats    - to run tests
#   ncurses - to colorize bats output
RUN apk add -q bash dash zsh git bats ncurses

# install sprompt
RUN set -eux; \
    mkdir "$MOUNTDIR"; \
    ln -sf "$MOUNTDIR/src/sprompt.sh" /etc/profile.d/ && \
    printf '\nexport USER="$(id -un)"' >> /etc/profile && \
    printf '\nsetopt PROMPT_SUBST' > /etc/zsh/zshrc
ENV PS1_REMOTE_SIGN_CONTAINER="󰡨"

# prepare container for debug
USER "$USERNAME"
RUN set -eux; \
    git init -qb 'main' "$HOMEDIR/repo" && \
    printf 'Just a git tracked file' > "$HOMEDIR/repo/tracked"
WORKDIR "$MOUNTDIR"
CMD [ "/bin/sh" ]
