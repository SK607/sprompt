# SPrompt
A lightweight prompt for POSIX shell (`bash`, `zsh`, `dash`, `ash`).

![Shell: Bash, Zsh, Dash, Ash](./media/supports-badge.svg)
[![License: MIT](./media/license-badge.svg)](./LICENSE)

![Features](media/examples.png)


## Installation
> NOTE: if you use zsh - add `setopt PROMPT_SUBST` to `.zshrc`

> NOTE: if you use large git repos - run `git config conr.utrackedcache true` to improve `git status` speed

### System-wide
Install sprompt for all users => preserve it on switching (e.g. on `su -`).

```bash
curl -o /tmp/sprompt.sh \
    https://codeberg.org/SK607/sprompt/raw/branch/main/src/sprompt.sh
sudo install -vm 644 /tmp/sprompt.sh /etc/profile.d
```

> NOTE: if `/etc/profile.d/*.sh` is not sourced - append `. /etc/profile.d/sprompt.sh` to `/etc/profile`


### User-wide
Install sprompt just for the current user.

```bash
curl -o /tmp/sprompt.sh \
    https://codeberg.org/SK607/sprompt/raw/branch/main/src/sprompt.sh
install -vm 644 /tmp/profile.sh ~/.config 
printf '\n%s\n' '. ~/.config/profile.sh' >> ~/.profile
```


## Configuration
Set corresponding environment variable in your `bashrc`, e.g.:
```bash
export PS1_ARROW_COLOR=161  # deep pink
```

### Foreground colors
Supports [256-color](https://www.ditig.com/256-colors-cheat-sheet).

Default values:
- `PS1_ARROW_COLOR=7` (system white)
- `PS1_REMOTE_COLOR=3` (system yellow)
- `PS1_USER_COLOR=7` (system white)
- `PS1_HOST_COLOR=7` (system white)
- `PS1_GIT_COLOR=2` (system green)
- `PS1_MY_DIR_COLOR=4` (system blue)
- `PS1_DIR_COLOR=1` (system red)
- `PS1_PYENV_COLOR=5` (system magenta)

### Signs
Default values:
- `PS1_REMOTE_SIGN_SSH=ssh`
- `PS1_REMOTE_SIGN_CONTAINER=pod`


## Testing
```bash
MOUNT='/home/sk607/sprompt'

# run linter
podman run --rm -v "${PWD}:${MOUNT}:ro" --workdir "$MOUNT" \
    docker.io/koalaman/shellcheck:latest \
    -x src/* tests/*

# run tests
podman build -t sprompt .
podman run --rm -v "${PWD}:${MOUNT}:ro" -it sprompt bats tests

# run container (debug)
podman run --rm -v "${PWD}:${MOUNT}:ro" -it sprompt bash -il
```
