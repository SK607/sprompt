#!/usr/bin/env bats


_clear_variables() {
    unset _PS1_GIT_ROOT
    unset _PS1_GIT_BRANCH
    unset _PS1_GIT_UNCOMMITTED
}

setup_file() {
    saved_pwd="$PWD"
    tmp_repo="$(command mktemp -d)"
    export TEST_REPO="$tmp_repo"
    export TEST_BRANCH='newbr'

    git init -b 'main' "$TEST_REPO"
    cd "$TEST_REPO" || exit
    git config user.email 'email'
    git config user.name 'name'
    git config commit.gpgsign false

    touch foo
    git add . && git commit -m 'message1'

    mkdir child
    touch child/foo2
    git add . && git commit -m 'message2'

    git branch "$TEST_BRANCH"

    cd "$saved_pwd" || exit  # otherwise setup() doesn't work
}

setup() {
    _clear_variables
    source ./src/sprompt.sh
}

teardown_file() {
    _clear_variables
    rm -rf "$TEST_REPO"
    unset TEST_REPO
    unset TEST_BRANCH
}


@test 'branch is empty when not in repo' {
    cd /
    _ps1_update

    test -z "$_PS1_GIT_BRANCH"
}

@test 'uncommitted is empty when not in repo' {
    cd /
    _ps1_update

    test -z "$_PS1_GIT_UNCOMMITTED"
}

@test 'branch is set when git init' {
    repo="${TEST_REPO}_git_init"
    mkdir "$repo" && cd "$repo" && git init -b 'main'
    _ps1_update

    test -n "$_PS1_GIT_BRANCH"

    rm -rf "$repo"
}

@test 'uncommitted is set when git init' {
    repo="${TEST_REPO}_git_init"
    mkdir "$repo" && cd "$repo" && git init -b 'main'
    _ps1_update

    test -n "$_PS1_GIT_UNCOMMITTED"

    rm -rf "$repo"
}

@test 'branch is set when in repo (root)' {
    cd "$TEST_REPO"
    _ps1_update

    test -n "$_PS1_GIT_BRANCH"
}

@test 'uncommitted is set when in repo (root)' {
    cd "$TEST_REPO"
    _ps1_update

    test -n "$_PS1_GIT_UNCOMMITTED"
}

@test 'branch is set when in repo (child)' {
    cd "$TEST_REPO/child"
    _ps1_update

    test -n "$_PS1_GIT_BRANCH"
}

@test 'uncommitted is set when in repo (child)' {
    cd "$TEST_REPO/child"
    _ps1_update

    test -n "$_PS1_GIT_UNCOMMITTED"
}

@test 'branch is cleared when cd /' {
    cd "$TEST_REPO"
    _ps1_update
    cd /
    _ps1_update

    test -z "$_PS1_GIT_BRANCH"
}

@test 'uncommitted is cleared when cd /' {
    cd "$TEST_REPO"
    _ps1_update
    cd /
    _ps1_update

    test -z "$_PS1_GIT_UNCOMMITTED"
}

@test 'uncommitted is updated when file is added' {
    cd "$TEST_REPO"
    _ps1_update
    prev_uncommitted="$_PS1_GIT_UNCOMMITTED"
    touch "$TEST_REPO/bar"
    _ps1_update

    test "$_PS1_GIT_UNCOMMITTED" != "$prev_uncommitted"

    rm -f "$TEST_REPO/bar"
}

@test 'uncommitted is updated when file is changed' {
    cd "$TEST_REPO"
    _ps1_update
    prev_uncommitted="$_PS1_GIT_UNCOMMITTED"
    echo 'content' > "$TEST_REPO/foo"
    _ps1_update

    test "$_PS1_GIT_UNCOMMITTED" != "$prev_uncommitted"

    git checkout "$TEST_REPO/foo"
}

@test 'branch is updated when HEAD is detached' {
    cd "$TEST_REPO"
    _ps1_update
    git checkout HEAD~1
    _ps1_update

    commit_hash="$(command git rev-parse --verify HEAD)"
    [[ "$commit_hash" == "$_PS1_GIT_BRANCH"* ]]

    git checkout main
}

@test 'branch is updated when switching to new branch' {
    cd "$TEST_REPO"
    _ps1_update
    git checkout "$TEST_BRANCH"
    _ps1_update

    test "$_PS1_GIT_BRANCH" == "$TEST_BRANCH"

    git checkout main
}

@test 'branch, uncommitted are cleared when repo is removed' {
    cd "$TEST_REPO"
    _ps1_update
    rm -rf "$TEST_REPO/.git"
    _ps1_update

    test -z "$_PS1_GIT_BRANCH"
    test -z "$_PS1_GIT_UNCOMMITTED"
}
