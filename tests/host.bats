#!/usr/bin/env bats

_clear_variables() {
    unset HOSTNAME
}

setup() {
    _clear_variables
    source ./src/sprompt.sh
    _ps1_init
}

teardown_file() {
    _clear_variables
}


@test 'hostname is set' {
    test -n "$HOSTNAME"
}

@test 'hostname equals /proc/sys/kernel/hostname' {
    test "$HOSTNAME" == "$(command cat /proc/sys/kernel/hostname)"
}
