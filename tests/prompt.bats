#!/usr/bin/env bats

_clear_variables() {
    unset PS1
    unset PROMPT
    unset PROMPT_COMMAND
}

setup() {
    _clear_variables
    source ./src/sprompt.sh
}

teardown_file() {
    _clear_variables
}

@test 'Nothing is set in non-interactive mode' {
    test -z "$PS1"
    test -z "$PROMPT"
    test -z "$PROMPT_COMMAND"
}

@test 'PS1 is set (sh)' {
    prompt=$(_ps1_sh)
    test -n "$prompt"
}

@test 'PS1 contains elements from sprompt (sh)' {
    prompt=$(_ps1_sh)
    [[ "$prompt" == *'└─>'* ]]
}

@test 'PS1 is set (bash)' {
    _ps1_bash
    test -n "$PS1"
}

@test 'PS1 contains elements from sprompt (bash)' {
    _ps1_bash
    [[ "$PS1" == *'└─>'* ]]
}

@test 'PROMPT_COMMAND contains sprompt (bash)' {
    _enable_sprompt
    [[ "$PROMPT_COMMAND" == *'_ps1_bash'* ]]
}
