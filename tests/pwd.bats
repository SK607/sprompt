#!/usr/bin/env bats

_clear_variables() {
    unset _PS1_PREV_PWD
    unset _PS1_MY_DIR
}

setup() {
    _clear_variables
    source ./src/sprompt.sh
    _ps1_update
}

teardown_file() {
    _clear_variables
}


@test 'previous PWD is set' {
    test -n "$_PS1_PREV_PWD"
}

@test 'previous PWD equals previous PWD (after cd)' {
    prev_pwd="$PWD"
    cd "$(command dirname "$PWD")"

    test "$_PS1_PREV_PWD" == "$prev_pwd"
}

@test 'previous PWD equals PWD (after update)' {
    prev_pwd="$PWD"
    cd "$(command dirname "$PWD")"
    _ps1_update

    test "$_PS1_PREV_PWD" == "$PWD"
}

@test 'PWD is owned by me for ~' {
    cd "$HOME"
    _ps1_update

    test "$_PS1_MY_DIR" = 'y'
}

@test 'PWD is not owned by me for /' {
    cd /
    _ps1_update

    test "$_PS1_MY_DIR" = 'n'
}
