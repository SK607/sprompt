#!/usr/bin/env bats

setup() {
    unset VIRTUAL_ENV_DISABLE_PROMPT
    source ./src/sprompt.sh
    _ps1_init
}


@test 'python venv prompt is disabled' {
    test "$VIRTUAL_ENV_DISABLE_PROMPT" -eq '1'
}
