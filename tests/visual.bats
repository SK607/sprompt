#!/usr/bin/env bats

@test 'visual inspection' { true; }

teardown_file() {
    test -d "$HOME/repo" && cd "$HOME/repo" || exit
    ash -lc 'set -eu; _ps1_init && _ps1_sh; echo ash' >&3
    dash -lc 'set -eu; _ps1_init && _ps1_sh; echo dash' >&3
    bash -ilc '_ps1_init && _ps1_bash && printf "${PS1@P}bash\n"' >&3
    zsh -ilc 'print -P "${PROMPT}zsh"' >&3
}
