#!/usr/bin/env bats

@test 'ash runs prompt' {
    ash -lc 'set -eu; _ps1_init && _ps1_sh'
}

@test 'dash runs prompt' {
    dash -lc 'set -eu; _ps1_init && _ps1_sh'
}

@test 'bash runs prompt' {
    bash -lc 'set -eu; _ps1_init && _ps1_bash && echo -ne "${PS1@P}"'
}

@test 'zsh runs prompt' {
    zsh -lc 'set -eu; _ps1_init && _ps1_sh'
}
