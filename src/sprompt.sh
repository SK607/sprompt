#!/usr/bin/env sh

_ps1_init() {
    _ps1_set_remote
    _ps1_set_hostname
    export VIRTUAL_ENV_DISABLE_PROMPT=1
}

_ps1_set_remote() {
    if test -n "${SSH_CLIENT:-}" || test -n "${SSH_TTY:-}"; then
        _PS1_REMOTE="${PS1_REMOTE_SIGN_SSH:-ssh} "
    elif test -f '/.dockerenv' || test -f '/run/.containerenv'; then
        _PS1_REMOTE="${PS1_REMOTE_SIGN_CONTAINER:-pod} "
    else
        _PS1_REMOTE=''
    fi
}

_ps1_set_hostname() {
    if test -z "$HOSTNAME"; then
        HOSTNAME="$(command cat /proc/sys/kernel/hostname 2>/dev/null)"
    fi
}


_ps1_update() {
    if test -x "$(command -v git)"; then
        _ps1_set_git_root
        _ps1_set_git_branch
        _ps1_set_git_uncommitted
    fi

    if test "$PWD" != "${_PS1_PREV_PWD:-unset}"; then
        _ps1_set_i_own_pwd
        _PS1_WD="$(command basename "$PWD")"
        _PS1_PREV_PWD="$PWD"
    fi
}

_ps1_set_git_root() {
    git_root=''
    if test "$PWD" = "${_PS1_PREV_PWD:-unset}"; then
        if test -r "$_PS1_GIT_ROOT/.git/HEAD"; then
            git_root="$_PS1_GIT_ROOT"
        elif test -r "$PWD/.git/HEAD"; then
            git_root="$PWD"
        fi
    else
        git_root="$(_ps1_find_git_root "$PWD")"
    fi
    _PS1_GIT_ROOT="$git_root"
}

_ps1_find_git_root() {
    search_path="$*"
    while test "$search_path" != '/'; do
        if test -r "$search_path/.git/HEAD"; then
            echo "$search_path"
            break
        fi
        search_path="$(command dirname "$search_path")"
    done
}

_ps1_set_git_branch() {
    git_branch=''
    if test -d "$_PS1_GIT_ROOT"; then
        git_branch="$(command git --no-optional-locks symbolic-ref --short HEAD 2>/dev/null)" || true
        if test -z "$git_branch"; then
            git_branch="$(command git --no-optional-locks rev-parse --short HEAD 2>/dev/null)" || true
        fi
    fi
    _PS1_GIT_BRANCH="$git_branch"
}

_ps1_set_git_uncommitted() {
    git_uncommitted=''
    if test -d "$_PS1_GIT_ROOT"; then
        git_uncommitted="$(command git --no-optional-locks status -unormal --porcelain --ignore-submodules 2>/dev/null | command wc -l)" || true
    fi
    _PS1_GIT_UNCOMMITTED="$git_uncommitted"
}

_ps1_set_i_own_pwd() {
    user="${USER:-$(id -un)}"
    owner="$(command stat -c '%U' "$PWD")"
    if test "$user" = "$owner"; then
        _PS1_MY_DIR='y'
    else
        _PS1_MY_DIR='n'
    fi
}


_ps1_sh() {
    _ps1_update

    ## LINE 1: ┌── remote user@host branch[uncommitted]:directory
    prompt="$(_ps1_add '' "${PS1_ARROW_COLOR:-7}" "\n┌── ")"
    prompt="$(_ps1_add "$prompt" "${PS1_REMOTE_COLOR:-3}" "$_PS1_REMOTE")"
    prompt="$(_ps1_add "$prompt" "${PS1_USER_COLOR:-7}" "$USER")"
    prompt="$(_ps1_add "$prompt" "${PS1_HOST_COLOR:-7}" "@$HOSTNAME ")"

    # git
    if test -n "$_PS1_GIT_BRANCH"; then
        prompt="$(_ps1_add "$prompt" "${PS1_GIT_COLOR:-2}" "$_PS1_GIT_BRANCH")"
        if test "${_PS1_GIT_UNCOMMITTED:-0}" != '0'; then
            prompt="${prompt}[${_PS1_GIT_UNCOMMITTED}]"
        fi
        prompt="${prompt}:"
    fi

    # directory
    if test "${_PS1_MY_DIR:-}" = 'y'; then
        color="${PS1_MY_DIR_COLOR:-4}"
    else
        color="${PS1_DIR_COLOR:-1}"
    fi
    prompt="$(_ps1_add "$prompt" "$color" "$_PS1_WD")"

    # python env
    if test -d "${VIRTUAL_ENV:-}"; then
        venv="$(command basename "$VIRTUAL_ENV")"
        prompt="$(_ps1_add "$prompt" "${PS1_PYENV_COLOR:-5}" "$venv")"
    fi

    ## LINE 2: └─>
    prompt="$(_ps1_add "$prompt" "${PS1_ARROW_COLOR:-7}" "\n└─>")"
    prompt="${prompt}$(_ps1_reset) "

    ## EXPORT
    printf '%b' "$prompt"
}

_ps1_add() {
    # Arguments:
    # $1 = current prompt value
    # $2 = color code
    # $3 = additional prompt part
    #
    # zsh and bash require escape sequences to set 0 width for non-printable characters
    #   bash uses \[ and \]
    #   zsh uses %{ and %}
    printf '%s%s\033[0;38;5;%sm%s%s' "$1" "${_PS1_ESCAPE_START:-}" "$2" "${_PS1_ESCAPE_END:-}" "$3"
}

_ps1_reset() {
    printf '%s\033[0m%s' "${_PS1_ESCAPE_START:-}" "${_PS1_ESCAPE_END:-}"
}

_ps1_bash() {
    # bash supports non-printable character escape sequence
    #   either directly (inside PS1 variable)
    #   or through @P substition
    prompt="$(_ps1_sh)"
    PS1="${prompt@P}"
}


_enable_sprompt() {
    # set shell prompt
    shell="$(command tr -d '\0' < /proc/$$/cmdline)"
    case "$shell" in
        *bash*)
            _PS1_ESCAPE_START='\['
            _PS1_ESCAPE_END='\]'
            PROMPT_COMMAND='_ps1_bash' ;;
        *zsh*)
            _PS1_ESCAPE_START='%{'
            _PS1_ESCAPE_END='%}'
            # shellcheck disable=SC2034,SC2016
            PROMPT='$(_ps1_sh)' ;;
        *)
            PS1='$(_ps1_sh)' ;;
    esac

    # initialize static variables
    _ps1_init
}


# exit if non-iteractive
case "$-" in
    *i*) _enable_sprompt ;;
    *) true ;;  # otherwise source fails in bats tests
esac
